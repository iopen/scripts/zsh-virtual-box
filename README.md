# zsh-v-box

Simple zsh script with command line aliases for Virtual Box.

###### Setup on MacOs:

* `sudo nano ~/.zshrc`

~~~~
# VirtualBox aliases #
if [ -f ~/.zsh_virtual_box ]
    then
        source ~/.zsh_virtual_box
    else
        print "404: ~/.zsh_virtual_box not found."
fi
# VirtualBox aliases #
~~~~
* add above script and copy `.zsh_virtual_box` file to user folder
* or change path to file in the script above
* restart terminal
* DONE

###### Usage:

* `vm [ARGS]`
* arguments: `status|start|connect|save|resume|reset|down|help`

Feel free to customize as per your needs !
